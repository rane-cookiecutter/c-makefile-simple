# Description

[Cookiecutter](https://github.com/audreyr/cookiecutter) template to generate
a basic C project that compiles with a single Makefile. In this template all files are
located in a single directory. There is no structure for unit tests or documentation.

To use the template run `cookiecutter https://gitlab.com/rane-cookiecutter/c-makefile-simple.git`

## Requirements

- GNU Make (version 4.2.1)
