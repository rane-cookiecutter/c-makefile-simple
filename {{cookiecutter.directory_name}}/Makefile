# Compiler to use
CC = gcc

# Include paths for header files
INCLUDES = -I.

# Compiler flags
CFLAGS = -Wall -Wextra -g $(INCLUDES) -std=gnu11

# extra gcc flags used to build dependency files
DEPFLAGS = -MMD -MP

# Paths to required libraries (-L flags)
LFLAGS =

# The specific libraries that project depends on (-l flags)
# don't inlclude lib part of library ie libtte should use -ltte
LIBS =

# All source files
SRCS = $(wildcard *.c)

# All object files
OBJS := $(SRCS:%.c=%.o)

# dependency files (used to rebuild when a header file changes)
DEPS := $(SRCS:%.c=%.d)

# name of executable
MAIN = {{cookiecutter.executable}}

## compile main executable
all: $(MAIN)

$(MAIN): $(OBJS)
	@echo "Compiling executable: $(MAIN)"
	$(CC) $(CFLAGS) $(LFLAGS) $(LIBS) -o $(MAIN) $(OBJS)

# Automatically builds all object files from source files
# -c option compiles but does not link (create object files)
# -o is output filename
$(OBJS): %.o : %.c
	@echo "Compiling object file: $@"
	$(CC) $(CFLAGS) $(DEPFLAGS) -c $< -o $@

# include dependency files, must be after first target
-include $(DEPS)

## delete all temporary files and executables if they exist
clean:
	@echo "Deleting temporary files..."
	rm -rf -- *.o *.d *.dSYM $(MAIN)

.PHONY: clean all test
