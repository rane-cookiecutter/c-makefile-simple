/**
 * @file main.c
 * @author {{cookiecutter.author}}
 **/

#include <stdio.h>
#include <stdlib.h>

int main(void)
{
    printf("Hello World!\n");
    return 0;
}
